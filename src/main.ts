import { createApp } from "vue";
import vuetify from "@/plugins/vuetify";
import App from "./App.vue";
import router from "@/plugins/router";
import store from "@/plugins/store";

// GLOBAL REGISTRATION FOR COMPONENT
import globalBaseComponentRegistration from "./plugins/globalBaseComponentRegistration/index";

const app = createApp(App);
globalBaseComponentRegistration(app);
app.use(router);
app.use(store);
app.use(vuetify);
app.mount("#app");
