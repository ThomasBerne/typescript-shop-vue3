import { App } from "vue";

export default function registration(app: App<Element>): void {
  function registrateFolder(
    baseComponentsContext: __WebpackModuleApi.RequireContext,
  ) {
    baseComponentsContext.keys().forEach((fileName: string) => {
      // Get component config
      const componentBaseConfig = baseComponentsContext(fileName);
      // Gets the file name regardless of folder depth
      const componentName: string | undefined = fileName
        ?.split("/")
        ?.pop()
        ?.replace(/\.\w+$/, "");

      // Register component globally
      app.component(
        componentName ? (componentName as string) : "",
        // Look for the component options on `.default`, which will
        // exist if the component was exported with `export default`,
        // otherwise fall back to module's root.
        componentBaseConfig.default || componentBaseConfig,
      );
    });
  }

  const coreBaseComponents = require.context(
    // The relative path of the components folder
    "@/components/UI",
    // Whether or not to look in subfoldersthis.$route
    true,
    // The regular expression used to match base component filenames
    /Base[A-Z]\w+\.(vue|js)$/,
  );

  registrateFolder(coreBaseComponents);
}
