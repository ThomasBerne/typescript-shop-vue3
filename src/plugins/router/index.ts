import { createRouter, createWebHistory, RouteRecordRaw } from "vue-router";
import Market from "@/views/Market.vue";
import Admin from "@/views/Admin/Admin.vue";

const routes: Array<RouteRecordRaw> = [
  {
    path: "/",
    name: "Market",
    component: Market,
  },
  {
    path: "/admin",
    name: "Admin",
    component: Admin,
  },
];

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes,
});

export default router;
