import { createStore, useStore as baseUseStore, Store } from "vuex";

import { State as CoreState } from "./core";
export type State = {
  core: CoreState;
};

export function useStore(): Store<State> {
  return baseUseStore();
}

import core from "./core";
export default createStore({
  modules: { core },
});
