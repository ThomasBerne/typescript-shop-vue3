import { Module } from "vuex";
import { State as RootState } from "@/plugins/store";

export interface State {
  sidebarStatus: boolean;
}

const core: Module<State, RootState> = {
  namespaced: true,
  state: {
    sidebarStatus: true,
  },
  mutations: {
    setSidebarStatus(state, payload: boolean) {
      state.sidebarStatus = payload;
    },
  },
};

export default core;
